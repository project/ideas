Drupal core ideas

Use this template for proposing new ideas:

<h2>Idea summary</h2>

<h3><em>What</em> is the problem to solve?</h3>
(Missing functionality X, prevent Y from happening, make Z easier to do)

<h3><em>Who</em> is this for?</h3>
(Evaluators, site visitors, content authors, site managers, (advanced) site builders, developers, core developers, site owners, other?)

<h3><em>Result:</em> what will be the outcome?</h3>
(Describe the result: how will this improve things for the stated audience(s)?)

<h3><em>How</em> can we know the desired result is achieved?</h3>
(Usability test, a metric to track, feedback)
